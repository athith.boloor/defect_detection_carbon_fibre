# Object detection on Carbon Fibres

## References

+ https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/

## To use google colab
### To  get the code 
+ get the source code folder from gitlab `!git clone -b dev https://git.rwth-aachen.de/athith.boloor/defect_detection_carbon_fibre.git` 

### Populate the pre trained model folder
+ run the code `!wget http://download.tensorflow.org/models/object_detection/ssd_inception_v2_coco_2018_01_28.tar.gz`
+ extract the file using the command `!tar xvzf ssd_inception_v2_coco_2018_01_28.tar.gz -C defect_detection_carbon_fibre/pre-trained-model/`
+ Cut/Move the content of this folder outside(one level above)`mv defect_detection_carbon_fibre/pre-trained-model/ssd_inception_v2_coco_2018_01_28/* defect_detection_carbon_fibre/pre-trained-model/`

### Populate the data
+ get the data stored in zip format from Amazon s3 `!wget https://thesis-master.s3-eu-west-1.amazonaws.com/project/images.zip`
+ unzip to the folder `images` using the following command `!unzip images.zip -d defect_detection_carbon_fibre/images/`


### Train and save the model
+ Change the directory from `/content` to `/content/defect_detection_carbon_fibre` by executing this code 
`import os`
`os.chdir(os.getcwd()+"/defect_detection_carbon_fibre/")`
+ execute the command `!python train.py`
+ first download the saved model (To carry out the inference later)and convert it into the zip format and save it in AmazonS3

## Inference

### Retrieve the trained model
+ retrieve the saved model from Amazon s3 using the command `!wget https://thesis-master.s3-eu-west-1.amazonaws.com/project/trained_model.zip`
+ unzip the model into the  directory `defect_detection_carbon_fibre/training/` using `!unzip trained_model.zip -d defect_detection_carbon_fibre/training/`

### Exporting a trained inference graph
+ change working directory to `/defect_detection_carbon_fibre/` using the command `import os` and`os.chdir(os.getcwd()+"/defect_detection_carbon_fibre/")`
+ run the following command to generate trained inference graph in the directory`trained-inference-graphs/output_inference_graph_v1.pb` using the following command `!python export_inference_graph.py --input_type image_tensor --pipeline_config_path training/ssd_inception_v2_coco.config --trained_checkpoint_prefix training/model.ckpt-20000 --output_directory trained-inference-graphs/output_inference_graph_v1.pb`

### Predict on test data
+ run `!python infer.py` to make predictions on the test images.
